# GPSFleet Logistic & Lorry Companies Web Scraper

## How to use

---

- git clone
  
- Navigate to the folder 
  
- Enter command below
    
> pip install -r requirements.txt

- Enter command below

  - For lorry companies details

    > py lorryScrape.py

  - For logistic companies details

    > py moverScrape.py

- You will get Lorry.xlsx / Logistic.xlsx depending on the file you run

---
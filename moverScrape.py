from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd

list_of_website = [
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=A",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=B",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=C",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=D",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=E",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=F",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=G",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=H",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=I",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=J",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=K",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=L",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=M",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=N",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=O",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=P",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=Q",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=R",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=S",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=T",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=U",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=V",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=W",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=X",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=Y",
    "http://www.msialogistics.com/category/lr/movers?classid=LM200&alpha=Z"
]

company_info_list = []

def scrape(website_list,company_info_list):
    for iteration in range(len(website_list)):
        driver = webdriver.Firefox()
        driver.get(website_list[iteration])
        content = driver.page_source

        soup = BeautifulSoup(content,'lxml')

        for a in soup.findAll("table",{"class":"BodyTxt"}):
            number_of_company_on_current_page = len(a.findAll("a",{"class": lambda L: L and L.startswith('link_txt')}))
            current_loop_company_info = []
            dict_keys = [ "company_name","company_phone_no","company_email","company_address","company_website"]
            i = 0

            # create list of dictionary
            while i < number_of_company_on_current_page:
                current_dict = {dict: None for dict in dict_keys}
                current_loop_company_info.append(current_dict)
                i += 1

            company_name_counter = 0
            company_phone_no_counter = 0
            company_email_counter = 0
            company_address_counter = 0
            company_website_counter = 0

            # Get Company name
            for nameScrape in a.findAll("a",{"class": lambda L: L and L.startswith('link_txt')}):
                for elem in nameScrape.contents:
                    company_name = elem
                    current_loop_company_info[company_name_counter]["company_name"] = company_name.replace('\xa0\xa0', '')
                    company_name_counter += 1
                    
            # Get Company phone number
            for phoneNumberScrape in a.findAll("a",{"id": lambda L: L and L.startswith('telno')}):
                company_phone_no = "No phone number"
                
                if not phoneNumberScrape.contents:
                    current_loop_company_info[company_phone_no_counter]["company_phone_no"] = company_phone_no
                    company_phone_no_counter +=1
                        
                for elem in phoneNumberScrape.contents:
                    company_phone_no = elem
                    if(current_loop_company_info[company_phone_no_counter]["company_phone_no"] is None):
                        current_loop_company_info[company_phone_no_counter]["company_phone_no"] = company_phone_no
                    else:
                        current_loop_company_info[company_phone_no_counter]["company_phone_no"] = str(current_loop_company_info[company_phone_no_counter]["company_phone_no"]) + "," + company_phone_no
                    company_phone_no_counter +=1

            # Get Company email
            for emailScrape in a.findAll("a",{"id": lambda L: L and L.startswith('email')}):
                company_email = "No email"
                
                if not emailScrape.contents:
                    current_loop_company_info[company_email_counter]["company_email"] = company_email
                    company_email_counter +=1
                        
                for elem in emailScrape.contents:
                    company_email = elem
                    if(current_loop_company_info[company_email_counter]["company_email"] is None):
                        current_loop_company_info[company_email_counter]["company_email"] = company_email
                    else:
                        current_loop_company_info[company_email_counter]["company_email"] = str(current_loop_company_info[company_email_counter]["company_email"]) + "," + company_email
                    company_email_counter +=1
            
            address = []
            # Get company address
            for addressScrape in a.findAll("span",{"id":lambda L: L and L.startswith('ContentPlaceHolder1_dgrdCompany_')}):

                for e in addressScrape.findAll('br'):
                    e.extract()  
            
                if addressScrape.contents != []:
                    if "Malaysia " in addressScrape.contents:
                        merge=","
                        current_loop_company_info[company_address_counter]["company_address"] = merge.join(addressScrape.contents)
                        company_address_counter += 1
                
            # Get company website_list
            websiteList = []
            for emailScrape in a.findAll("a",{"id":lambda L: L and L.startswith('aweb')}):
                current_loop_company_info[company_website_counter]["company_website"] = str(emailScrape.get('title'))
                company_website_counter += 1

            # print(current_loop_company_info)
            company_info_list = company_info_list + current_loop_company_info

        driver.quit()
        iteration += 1
    return company_info_list

final = scrape(list_of_website,company_info_list)

df = pd.DataFrame.from_dict(final)
df.to_excel('Logistic.xlsx')

